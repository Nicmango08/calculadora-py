# Calculator Program
#Functions
def sumar(x, y):
    return x + y

def restar(x, y):
    return x - y

#Menu
print("Seleccionar Operaación")
print("1. Sumar")
print("2. Restar")

choice = input("Ingrese operacion(1, 2, 3, 4): ")
num1 = int(input("Ingrese el primer numero: "))
num2 = int(input("Ingrese el segundo numero: "))

if choice == '1':
    print(num1, " + ", num2, " = ", sumar(num1,num2))

elif choice == '2':
    print(num1, " - ", num2, " = ", restar(num1,num2))

elif choice == '3':
    print(" AUN NO ESTA IMPLEMENTADO ")

elif choice == '4':
    print(" AUN NO ESTA IMPLEMENTADO ")

else:
    print(" **************DATO INVALIDO ***************** ")    
